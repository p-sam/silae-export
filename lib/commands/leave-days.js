const fs = require('node:fs');
const util = require('node:util');
const {default: ical} = require('ical-generator');
const BaseClientCommand = require('./base-client.js');

const GROUP_TYPES = ['ME', 'TEAM', 'COMPANY'];

const FORMATTERS = {
	ics(events) {
		const cal = ical();

		for(const event of events) {
			cal.createEvent({
				start: new Date(event.start),
				end: new Date(event.end),
				summary: `!${event.employeeName} - ${event.label}`,
				description: JSON.stringify(event, null, 4),
				organiser: 'silae-export',
				url: 'https://my.silae.fr'
			});
		}

		return cal.toString();
	},
	json: result => JSON.stringify(result),
	console: (result, stream) => util.inspect(result, {colors: stream.hasColors && stream.hasColors()}) + '\n'
};

class LeaveDaysCommand extends BaseClientCommand {
	getName() {
		return 'leave-days';
	}

	getDescription() {
		return 'Leave days export';
	}

	configure(yargs) {
		return yargs
			.option('group', {
				alias: 'g',
				choices: GROUP_TYPES,
				default: GROUP_TYPES[0],
				required: true
			})
			.option('company-id', {
				alias: 'c',
				default: '',
				defaultDescription: 'first'
			})
			.option('min-date', {
				coerce: BaseClientCommand.validator('min-date', 'date'),
				required: true
			})
			.option('max-date', {
				coerce: BaseClientCommand.validator('max-date', 'date'),
				required: true
			})
			.option('format', {
				alias: 'f',
				choices: Object.keys(FORMATTERS),
				default: 'console',
				coerce: string => String(string).toLowerCase()
			})
			.option('out', {
				alias: 'o',
				defaultDescription: 'stdout'
			});
	}

	async run({group, companyId, minDate, maxDate, format, out}) {
		const client = await this.getSilaeClient();

		companyId ||= client.listCompanies()[0].companyId;

		const stream = out ? fs.createWriteStream(out) : process.stdout;

		const result = await client.listLeaveDays(companyId, {groupType: group, start: minDate, end: maxDate});

		const output = await FORMATTERS[format](result, stream);

		await new Promise((resolve, reject) => {
			stream.end(output, 'utf8', resolve);
			stream.once('error', reject);
		});

		return 0;
	}
}

module.exports = LeaveDaysCommand;
