const fs = require('node:fs');
const path = require('node:path');

const BaseClientCommand = require('./base-client.js');

const fsExists = p => fs.promises.access(p, fs.constants.F_OK)
	.then(() => true)
	.catch(() => false);

class PayslipsCommand extends BaseClientCommand {
	getName() {
		return 'payslips';
	}

	getDescription() {
		return 'Payslips PDF export';
	}

	configure(yargs) {
		return yargs
			.option('force', {
				alias: 'f',
				type: 'boolean'
			})
			.option('company-id', {
				alias: 'c',
				default: '',
				defaultDescription: 'first'
			})
			.option('out-dir', {
				alias: 'o',
				default: '',
				defaultDescription: 'cwd',
				coerce: path.resolve,
				required: true
			})
			.option('min-date', {
				coerce: BaseClientCommand.validator('min-date', 'date')
			})
			.option('max-date', {
				coerce: BaseClientCommand.validator('max-date', 'date')
			});
	}

	async run({outDir, force, minDate, maxDate, companyId}) {
		const client = await this.getSilaeClient();

		companyId ||= client.listCompanies()[0].companyId;

		for(const payslip of await client.listPayslips(companyId)) {
			if(minDate && payslip.at < minDate) {
				continue;
			}

			if(maxDate && payslip.at > maxDate) {
				continue;
			}

			const pdfPath = path.join(outDir, `./${payslip.at}.pdf`);

			if(!force && await fsExists(pdfPath)) {
				continue;
			}

			const pdfBuffer = await client.payslipFile(companyId, payslip.id, payslip.nature);
			await fs.promises.writeFile(pdfPath, pdfBuffer);

			console.log(pdfPath);
		}

		return 0;
	}
}

module.exports = PayslipsCommand;
