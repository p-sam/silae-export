const SilaeClient = require('../silae-api.js');
const BaseCommand = require('./base.js');

class BaseClientCommand extends BaseCommand {
	#client;
	#clientArgv;

	yargsSpec() {
		const [name, desc, configure, run] = super.yargsSpec();
		return [
			name,
			desc,
			yargs => configure(this.clientConfigure(yargs)),
			argv => {
				this.#clientArgv = [argv.login, argv.password];
				delete argv.login;
				delete argv.password;
				delete argv.l;
				delete argv.p;
				return run(argv);
			}
		];
	}

	async getSilaeClient() {
		this.#client ||= new SilaeClient();

		if(!this.#client.loggedIn && this.#clientArgv) {
			await this.#client.login(...this.#clientArgv);
			this.#clientArgv = null;
		}

		return this.#client;
	}

	clientConfigure(yargs) {
		return yargs
			.option('login', {
				alias: 'l',
				required: true
			})
			.option('password', {
				alias: 'p',
				required: true
			});
	}
}

module.exports = BaseClientCommand;
