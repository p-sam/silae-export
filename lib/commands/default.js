const BaseCommand = require('./base.js');

class DefaultCommand extends BaseCommand {
	getName() {
		return '*';
	}

	getDescription() {
		return false;
	}

	run() {
		this._yargs.showHelp();
		return 1;
	}
}

module.exports = DefaultCommand;
