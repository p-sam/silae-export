module.exports = {
	default: require('./default.js'),
	companies: require('./companies.js'),
	payslips: require('./payslips.js'),
	leaveDays: require('./leave-days.js')
};
