const VALIDATORS = {
	date(string) {
		if(!string) {
			return undefined;
		}

		if(!/^\d{4}-\d{2}-\d{2}$/.test(string)) {
			throw new TypeError('invalid date ' + JSON.stringify(string) + ': must be YYYY-MM-DD');
		}

		return string;
	}
};

class BaseCommand {
	constructor(yargs) {
		this._yargs = yargs;
	}

	static validator(name, type) {
		let fn;

		if(typeof(type) === 'function') {
			fn = type;
		} else if(type in VALIDATORS) {
			fn = VALIDATORS[type];
		} else {
			throw new Error('Unknown validator: ' + String(type));
		}

		return arg => {
			try {
				return fn(arg);
			} catch (error) {
				throw new Error([name, error.message].join(': '));
			}
		};
	}

	yargsSpec() {
		return [
			this.getName(),
			this.getDescription(),
			yargs => this.configure(yargs),
			async argv => {
				this._yargs.exit(await this.failSafeRun(argv));
			}
		];
	}

	getName() {
		return 'Command';
	}

	getDescription() {
		return '';
	}

	configure(yargs) {
		return yargs;
	}

	async failSafeRun(argv) {
		let rc;

		try {
			rc = await this.run(argv);
		} catch (error) {
			console.log(error);
			rc = 1;
		}

		return rc;
	}

	async run() {
		return 0;
	}
}

module.exports = BaseCommand;
