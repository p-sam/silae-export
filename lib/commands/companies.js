const fs = require('node:fs');
const util = require('node:util');
const BaseClientCommand = require('./base-client.js');

const FORMATTERS = {
	json: result => JSON.stringify(result),
	console: (result, stream) => util.inspect(result, {colors: stream.hasColors && stream.hasColors()}) + '\n'
};

class CompaniesCommand extends BaseClientCommand {
	getName() {
		return 'companies';
	}

	getDescription() {
		return 'List companies';
	}

	configure(yargs) {
		return yargs
			.option('format', {
				alias: 'f',
				choices: Object.keys(FORMATTERS),
				default: 'console',
				coerce: string => String(string).toLowerCase()
			})
			.option('out', {
				alias: 'o',
				defaultDescription: 'stdout'
			});
	}

	async run({format, out}) {
		const client = await this.getSilaeClient();

		const stream = out ? fs.createWriteStream(out) : process.stdout;

		const companies = client.listCompanies();

		const output = await FORMATTERS[format](companies, stream);

		await new Promise((resolve, reject) => {
			stream.once('error', reject);
			stream.end(output, 'utf8', resolve);
		});

		return 0;
	}
}

module.exports = CompaniesCommand;
