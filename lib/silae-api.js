const assert = require('node:assert/strict');

async function httpCall(url, options = {}, responseType) {
	const response = await fetch(url, options);
	if(!response.ok) {
		throw new Error(`${url}: [${response.status}] ${await response.text()}`);
	}

	return responseType ? response[responseType]() : response;
}

class SilaeClient {
	#cookies;
	#ctx;

	get loggedIn() {
		return (this.#cookies && this.#ctx);
	}

	async login(username, password) {
		const auth = await httpCall('https://signin.my.silae.fr/d4f38610-a107-482b-a240-c9ed73f434b4/oauth2/v2.0/token?p=B2C_1A_MYSILAE_HEADLESS_SIGNIN', {
			method: 'POST',
			headers: {
				'Content-Type': 'application/x-www-form-urlencoded'
			},
			body: new URLSearchParams({
				username,
				password,
				client_id: '467f7b0b-7dbe-4139-95f5-46a2d50e693d',
				scope: 'openid 467f7b0b-7dbe-4139-95f5-46a2d50e693d',
				response_type: 'token id_token',
				grant_type: 'password'
			}).toString()
		}, 'json');

		assert('access_token' in auth);
		const ctxResponse = await httpCall('https://api.my.silae.fr/v1/authenticate/bearer?type=v2', {
			headers: {
				Authorization: 'Bearer ' + auth.access_token
			}
		});

		assert(ctxResponse.headers.has('set-cookie'));
		this.#ctx = await ctxResponse.json();

		this.#cookies = ctxResponse.headers.getSetCookie().map(c => c.split(';')[0]).join(';');
	}

	#apiGet(route, query = {}, responseType = 'json') {
		assert(this.#cookies);

		const url = new URL(route, 'https://api.my.silae.fr/');
		for(const [k, v] of Object.entries(query)) {
			url.searchParams.set(k, v);
		}

		return httpCall(url, {
			headers: {
				Cookie: this.#cookies
			}
		}, responseType);
	}

	listCompanies() {
		assert(this.#ctx && 'companies' in this.#ctx);
		return JSON.parse(JSON.stringify(this.#ctx.companies));
	}

	listPayslips(companyId) {
		return this.#apiGet(`/v1/companies/${companyId}/payslips`);
	}

	listLeaveDays(companyId, {start, end, groupType} = {}) {
		return this.#apiGet(`/v1/companies/${companyId}/leave-days-details`, {start, end, groupType});
	}

	async payslipFile(companyId, payslipId, nature) {
		const arrayBuf = await this.#apiGet(`/v1/companies/${companyId}/payslips/${payslipId}/file`, {nature}, 'arrayBuffer');
		return Buffer.from(arrayBuf);
	}
}

module.exports = SilaeClient;
