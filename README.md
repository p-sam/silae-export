silae-export
============

## Help

```
# .env
export SILAE_LOGIN=user@example.org
export SILAE_PASSWORD=ChangeMe

$ npm start -- --help
silae-export

Commandes :
  silae-export bulletin  Bulletin PDF export
  silae-export absence   Absence export

Options :
  --help     Affiche l'aide                                                  [?]
  --version  Affiche le numéro de version                                    [?]
```

## Bulletin

```
$ npm start -- bulletin --help
silae-export bulletin

Bulletin PDF export

Options :
      --help      Affiche l'aide                                             [?]
      --version   Affiche le numéro de version                               [?]
  -l, --login                                                           [requis]
  -p, --password                                                        [requis]
  -f, --force                                                                [?]
  -o, --out-dir                                      [requis] [défaut : courant]
      --min-date
      --max-date
```

## Absence

```
$ npm start -- absence --help
silae-export absence

Absence export

Options :
      --help      Affiche l'aide                                             [?]
      --version   Affiche le numéro de version                               [?]
  -l, --login                                                           [requis]
  -p, --password                                                        [requis]
  -t, --type                       [requis] [choix : "MOI", "EQUIPE", "DOSSIER"]
      --min-date                                                        [requis]
      --max-date                                                        [requis]
      --format           [choix : "ics", "json", "console"] [défaut : "console"]
  -o, --out                                                    [défaut : stdout]
```
