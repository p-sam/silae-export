#!/usr/bin/env node

require('dotenv').config();

process.env.TZ = 'UTC';

const yargs = require('yargs/yargs');
const {hideBin} = require('yargs/helpers');

const commands = require('../lib/commands/index.js');

const yargsInstance = yargs(hideBin(process.argv))
	.env('SILAE')
	.strictCommands()
	.updateStrings({
		boolean: '?'
	});
for(const CmdClass of Object.values(commands)) {
	const cmd = new CmdClass(yargsInstance);
	yargsInstance.command(...cmd.yargsSpec());
}

yargsInstance.parse();
